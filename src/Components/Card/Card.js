import "./Card.css";
import sunnyPNG from "../../Media/icon/Vector.png";
import { useEffect, useState } from "react";

function Card({ newData, index, trueFalseArr }) {
  const [active, setActive] = useState(false);

  // useEffect(() => {
  //   setCounter((counter2) => {
  //     return counter2 + 1;
  //   });
  // }, [active]);

  useEffect(() => {
    setActive(trueFalseArr[index]);
  }, [trueFalseArr]);

  return (
    <div
      className="card"
      style={active ? { scale: "1.1" } : {}}
      onClick={() => {
        setActive((active) => {
          return !active;
        });
      }}
    >
      <div className="city">{newData.city}</div>
      <div style={{ marginBottom: "20px" }}>
        <img src={sunnyPNG} alt="sunny" />
      </div>
      <div className="weather">{newData.weather}</div>
      <div className="weather">
        {(Math.sign(newData.degree) === 1 ? "+" : "") + newData.degree + "°С"}
      </div>
      <div>{index + 1}</div>
      <div>{JSON.stringify(trueFalseArr[index])}</div>
    </div>
  );
}

export default Card;

//монго тегов - вёрстка
