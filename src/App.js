import { useState } from "react";
import "./App.css";
import Card from "./Components/Card/Card";

const data = [
  { city: "Москва", weather: "Солнечно", degree: 12 },
  { city: "Владимир", weather: "Дождь", degree: 10 },
  { city: "Питер", weather: "Гроза", degree: 7 },
];

function App() {
  const [trueFalse, setTrueFalse] = useState([false, false, false]);

  return (
    <div className="App">
      <div className="box">
        {data.map((newDataqq, index) => {
          return (
            <Card newData={newDataqq} index={index} trueFalseArr={trueFalse} />
          );
        })}
      </div>
      <div
        className="btn"
        onClick={() => {
          setTrueFalse((curTrueFalse) => {
            return curTrueFalse.map((state1) => {
              return Math.random() < 0.5;
            });
          });
        }}
      >
        Перемешать
      </div>
    </div>
  );
}

export default App;
